#!/bin/sh

# Exit the script if there is an error
set -euo pipefail

# If not arguments are supplied, start vale with the defaults
if [ $# -eq 0 ]; then
  vale /app/content
fi

# Now running any arbitrary commands that might have been passed in
exec "$@"
