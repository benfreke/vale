FROM jdkato/vale:v2.2.0

ENV VALE_VERSION 2.2.0

ENV STYLE_GUIDE_GOOGLE_VERSION 0.3.1

RUN wget https://github.com/errata-ai/Google/releases/download/v${STYLE_GUIDE_GOOGLE_VERSION}/Google.zip \
    && mkdir /styles/ \
    && unzip -d /styles/ Google.zip \
    && rm Google.zip

COPY entrypoint.sh /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

COPY vale.ini /app/.vale.ini

WORKDIR /app/content
